package dat.com.friday_13th;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    EditText year_to,year_from;
    TextView fridays;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fridays=(TextView)findViewById(R.id.friday_tv);
        year_to=(EditText)findViewById(R.id.second_year);
        year_from=(EditText)findViewById(R.id.first_year);
        String FirstInput = "01/01/2015";
        String SecondInput= "20/06/2016";
        year_to.setText(SecondInput);
        year_from.setText(FirstInput);

        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        Calendar first=Calendar.getInstance();
        try {
            first.setTime(dateFormat.parse(FirstInput));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar second=Calendar.getInstance();
        try {
            second.setTime(dateFormat.parse(SecondInput));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ArrayList<Date> all_friday=new ArrayList<>();

        while(!first.equals(second)){
            first.add(Calendar.DATE, 1);
            if(first.get(Calendar.DAY_OF_WEEK)==Calendar.FRIDAY && first.get(Calendar.DAY_OF_MONTH)==13){
                all_friday.add(first.getTime());

            }
        }

        System.out.println("FRIDAY" + all_friday);
        System.out.println("TODAY"+Calendar.DAY_OF_MONTH);





    }
}
